package pe.uni.davisalderetev.pc4;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.SharedPreferences;

import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class PlatoElegido extends AppCompatActivity {
    ImageView imageViewDish;
    Spinner spinnerNum;
    EditText editTextName,editTextDir;
    RadioButton radioButtonV,radioButtonM;
    Button buttonSend;
    TextView textViewTitle;
    int cant=0;
    String nameCreditC;
    boolean valVisa=false;
    boolean valMas=false;
    ArrayAdapter<CharSequence> adapter;
    LinearLayout linearLayout;

    //---guardado de data
    SharedPreferences sharedPreference;
    String nameSave;
    String dirSave;
    Boolean creCardSaveV;
    Boolean creCardSaveM;
    int cSave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plato_elegido);

        imageViewDish=findViewById(R.id.image_view_logo);
        spinnerNum=findViewById(R.id.spinner);
        editTextName=findViewById(R.id.edit_text_name_dest);
        editTextDir=findViewById(R.id.edit_text_dir);
        buttonSend=findViewById(R.id.button_envio);
        textViewTitle=findViewById(R.id.text_acti2);
        radioButtonV=findViewById(R.id.radio_button_V);
        radioButtonM=findViewById(R.id.radio_button_M);

        Intent intent=getIntent();
        //String num=intent.getStringExtra("PLATO");
        String tit=intent.getStringExtra("TITLE");
        textViewTitle.setText(tit);
        //imageViewDish.setImageResource(num1);

        adapter=ArrayAdapter.createFromResource(this,R.array.cantidad, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNum.setAdapter(adapter);

        spinnerNum.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                        cant=1;
                }
                if(position==1){
                    cant=2;
                }
                if(position==2){
                    cant=3;
                }
                if(position==3){
                    cant=4;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if(radioButtonV.isChecked()){
            nameCreditC="Visa";
            valVisa=true;
        }
        if(radioButtonM.isChecked()){
            nameCreditC="MasterCard";
            valMas=true;
        }

        /*
        Snackbar.make(linearLayout,R.string.msg_snack_bar,Snackbar.LENGTH_INDEFINITE).setAction(R.string.button_snack_bar_text, v1 -> {

        }).show()
        * */
        buttonSend.setOnClickListener(v -> {
            if(editTextName.getText().toString().equals("") || editTextDir.getText().toString().equals("") || cant==0 || (valMas==true && valVisa==true) ){
                Snackbar.make(linearLayout,R.string.snack_msg,Snackbar.LENGTH_LONG).show();
            }else{
                AlertDialog.Builder builder=new AlertDialog.Builder(PlatoElegido.this);
                builder.setTitle(R.string.title_msg_dialog);
                builder.setCancelable(false);
                builder.setMessage(String.format(getResources().getString(R.string.msg_pedido_all),tit,editTextName.getText().toString(),cant));

                builder.setPositiveButton("ACEPTO", (dialog, which) -> {
                    //liberar memoria
                    moveTaskToBack(true);
                    //liberar el proceso
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                });
                builder.setNegativeButton("NO", (dialog, which) -> {

                });
                builder.create().show();
            }
        });
        retrieveData();

    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    private void saveData() {
        sharedPreference = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        nameSave = editTextName.getText().toString();
        dirSave = editTextDir.getText().toString();
        creCardSaveM = radioButtonM.isChecked();
        creCardSaveV=radioButtonV.isChecked();
        cSave=spinnerNum.getSelectedItemPosition();

        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString("key name", nameSave);
        editor.putString("key message", dirSave);
        editor.putInt("key counter", cSave);
        editor.putBoolean("key visa", creCardSaveV);
        editor.putBoolean("key master", creCardSaveM);
        editor.apply();

        Toast.makeText(getApplicationContext(), "Tus datos están guardados", Toast.LENGTH_LONG).show();
    }

    private void retrieveData() {
        sharedPreference = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        nameSave = sharedPreference.getString("key name", null);
        dirSave = sharedPreference.getString("key message", null);
        creCardSaveM = sharedPreference.getBoolean("key visa", false);
        creCardSaveV = sharedPreference.getBoolean("key master", false);
        cSave=sharedPreference.getInt("key counter",0);

        editTextName.setText(nameSave);

    }


}