package pe.uni.davisalderetev.pc4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> textTitle;
    ArrayList<Integer> image;
    ArrayList<String> textDesc;

    public GridAdapter(Context context, ArrayList<String> textTitle, ArrayList<Integer> image,ArrayList<String> textDesc) {
        this.context = context;
        this.textTitle = textTitle;
        this.image = image;
        this.textDesc=textDesc;

    }


    @Override
    public int getCount() {
        return textTitle.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        @SuppressLint("ViewHolder") View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view_layout,parent,false);
        TextView textViewTitle=view.findViewById(R.id.title_image);
        ImageView imageView=view.findViewById(R.id.image_view_food);
        TextView textViewDesc=view.findViewById(R.id.description_image);

        textViewTitle.setText(textTitle.get(position));
        imageView.setImageResource(image.get(position));

        textViewDesc.setText(textDesc.get(position));
        return view;
    }
}
