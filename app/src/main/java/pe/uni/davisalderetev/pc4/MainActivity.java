package pe.uni.davisalderetev.pc4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;
//import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    GridView gridView;
    ArrayList<String> textTitle=new ArrayList<>();
    ArrayList<Integer> image=new ArrayList<>();
    ArrayList<String> textDesc=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        gridView=findViewById(R.id.grid_view_main);
        fillArray();

        GridAdapter gridAdapter=new GridAdapter(this,textTitle,image,textDesc);
        gridView.setAdapter(gridAdapter);
        Intent intent= new Intent(MainActivity.this,PlatoElegido.class);
        gridView.setOnItemClickListener((parent,view,position,id)-> {
            intent.putExtra("PLATO",image.get(position));
            intent.putExtra("TITLE",textTitle.get(position));
            startActivity(intent);
            finish();
                }



        );


    }

    private void fillArray(){
        textTitle.add("AeroPuerto");
        textTitle.add("Caldo");
        textTitle.add("Casera");
        textTitle.add("Ceviche");
        textTitle.add("Chicharron");
        textTitle.add("Estofado");
        textTitle.add("Lomo Saltado");
        textTitle.add("Pollo");



        image.add(R.drawable.aero);
        image.add(R.drawable.caldo);
        image.add(R.drawable.casera);
        image.add(R.drawable.cevi);
        image.add(R.drawable.chichar);
        image.add(R.drawable.estofa);
        image.add(R.drawable.lomo);
        image.add(R.drawable.pollo);


        textDesc.add("Arroz con vegetale");
        textDesc.add("Delicioso Caldo");
        textDesc.add("Comida casera (Casa)");
        textDesc.add("Rico ceviche con aji");
        textDesc.add("Crocante y delicioso");
        textDesc.add("COn la mejor carne");
        textDesc.add("Lomo fino");
        textDesc.add("Rostizado a fuego lento");



    }
}